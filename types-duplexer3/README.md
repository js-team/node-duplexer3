# Installation
> `npm install --save @types/duplexer3`

# Summary
This package contains type definitions for duplexer3 (https://github.com/floatdrop/duplexer3).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/duplexer3.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/duplexer3/index.d.ts)
````ts
// Type definitions for duplexer3 0.1
// Project: https://github.com/floatdrop/duplexer3
// Definitions by: BendingBender <https://github.com/BendingBender>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node" />
import * as stream from 'stream';

export = duplexer3;

declare function duplexer3(writableStream: NodeJS.WritableStream, readableStream: NodeJS.ReadableStream): stream.Duplex;
declare function duplexer3(options: duplexer3.Options, writableStream: NodeJS.WritableStream, readableStream: NodeJS.ReadableStream): stream.Duplex;

declare namespace duplexer3 {
    interface Options extends stream.DuplexOptions {
        bubbleErrors?: boolean | undefined;
    }
}

````

### Additional Details
 * Last updated: Tue, 06 Jul 2021 20:32:49 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [BendingBender](https://github.com/BendingBender).
